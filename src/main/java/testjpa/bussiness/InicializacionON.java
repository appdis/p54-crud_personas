package testjpa.bussiness;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import testjpa.dao.TipoTelefonoDAO;
import testjpa.modelo.TipoTelefono;

@Startup
@Singleton
public class InicializacionON {
	
	@Inject
	private TipoTelefonoDAO daoTipoTel;
	
	
	@PostConstruct
	public void init() {
		
		System.out.println("inicializandoooooooooooooooooo");
		
		List<TipoTelefono> telefonos = daoTipoTel.getTipoTelefonos();
		if(telefonos.size()==0) {
			TipoTelefono tt1 = new TipoTelefono();
			tt1.setNombre("Convencional");
			daoTipoTel.insert(tt1);
			
			
			TipoTelefono tt2 = new TipoTelefono();
			tt2.setNombre("Celular");
			daoTipoTel.insert(tt2);
		}
		
		
	}

}
