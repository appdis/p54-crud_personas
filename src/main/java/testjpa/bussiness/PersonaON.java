package testjpa.bussiness;

import java.util.List;

import javax.ejb.Singleton;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.inject.Inject;

import testjpa.dao.PersonaDAO;
import testjpa.dao.TipoTelefonoDAO;
import testjpa.modelo.Persona;
import testjpa.modelo.TipoTelefono;

@Stateless
public class PersonaON {

	@Inject
	private PersonaDAO dao;
	
	@Inject
	private TipoTelefonoDAO daoTipoTel;
	
	public void guardar(Persona p) throws Exception{
		
		if(p.getNombre().length()<5)
			throw new Exception("Dimension corta");
		
		
		dao.save(p);		
	}
	
	public List<Persona> getListadoPersonas(){
		return dao.getPersonas();
	}
	
	public void borrar(int codigo) throws Exception {
		try {
			dao.delete(codigo);
		}catch(Exception e) {
			throw new Exception("Error al borrar " + e.getMessage());
		}
		
	}
	
	public Persona getPersona(int codigo) {
		Persona aux = dao.read(codigo);
		
		return aux;
		
	}
	
	/**
	 * Devuelve de la base de datos un objeto TipoTelefono a 
	 * partir de su código o PK
	 * 
	 * @param codigo		ID de tipo telefono buscado
	 * @return				Objeto TipoTelefono buscado
	 * @throws Exception 
	 */
	public TipoTelefono buscarTipoTelefono(int codigo) throws Exception {
		try {
			TipoTelefono tt = daoTipoTel.read(codigo);
			return tt;
		}catch(Exception e) {
			throw new Exception("Código no corresponde a un TT");
		}
	}
	
	
}
