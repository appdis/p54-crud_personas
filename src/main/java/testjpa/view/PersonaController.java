package testjpa.view;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import testjpa.bussiness.PersonaON;
import testjpa.modelo.Persona;
import testjpa.modelo.Telefono;
import testjpa.modelo.TipoTelefono;

@ManagedBean
@ViewScoped
public class PersonaController {

	private int edad;

	private Persona persona = new Persona();
	
	private int id;		//código para inicializacion en caso de edicion
	
	private List<Persona> listadoPersonas;
	
	@Inject
	private FacesContext fc;
	
	@Inject
	private PersonaON pON;
	
	@PostConstruct
	public void init() {
		System.out.println("init " + persona);
		listadoPersonas = pON.getListadoPersonas();
	}
	
	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}
	
	public List<Persona> getListadoPersonas() {
		return listadoPersonas;
	}

	public void setListadoPersonas(List<Persona> listadoPersonas) {
		this.listadoPersonas = listadoPersonas;
	}
	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String cargarDatos() {
		
		try {
			pON.guardar(persona);
			init();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public void loadDatos() {
		if(id==0)
			return;
		System.out.println("codigo editar " + this.id);
		persona = pON.getPersona(this.id);
		
		if(persona == null) {
			persona = new Persona();
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, 
					"Registro no existe", "Información");
			fc.addMessage(null, msg);
			
		}
			
		
		System.out.println(persona);
	}
	
	public String editar(int codigo) {
		/*System.out.println("codigo editar " + codigo);
		persona = pON.getPersona(codigo);
		System.out.println(persona);*/
		
		return "personas?faces-redirect=true&id="+codigo;
	}
	
	public String borrar(int codigo) {
		System.out.println("codigo borrar " + codigo);
		
		try {
			pON.borrar(codigo);
			init();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Error "+ e.getMessage());
			e.printStackTrace();
		}
		
		return null;
	}
	
	
	public String addTelefono() {
		System.out.println("add telefono");
		persona.addTelefono(new Telefono());
		
		System.out.println("size 2: " + persona.getTelefonos().size());
		
		return null;
	}
	
	public String buscarTelefono(Telefono telefono) {
		System.out.println("buscando telefono " + telefono);
		try {
			TipoTelefono tt = pON.buscarTipoTelefono(telefono.getCodigoTipoTemporal());
			System.out.println(tt);
			telefono.setTipo(tt);
		} catch (Exception e) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_FATAL, 
					e.getMessage(), "Error");
			fc.addMessage(null, msg);
		}
		
		return null;
	}
	
	
	
}
