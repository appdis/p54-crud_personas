package testjpa.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import testjpa.modelo.Persona;

@Stateless
public class PersonaDAO {
	
	
	@Inject
	private EntityManager em;
	
	public void save(Persona p) {
		if(this.read(p.getCodigo())!=null)
			this.update(p);
		else
			this.create(p);
		
	}

	
	public void create(Persona p) {
		em.persist(p);
	}
	
	public Persona read(int id) {
		return em.find(Persona.class, id);
	}
	
	public void update(Persona p) {
		
		em.merge(p);
	}
	
	public void delete(int id) {
		Persona p = read(id);
		em.remove(p);
	}
	
	public List<Persona> getPersonas(){
		String jpql = "SELECT p FROM Persona p ";
		
		Query q = em.createQuery(jpql, Persona.class);
		
		List<Persona> personas = q.getResultList();
		return personas;
	}
	
	public List<Persona> getPersonasPorNombre(String filtroBusqued){
		String jpql = "SELECT p FROM Persona p "
					+ "	WHERE p.nombre LIKE :filtro ";
		
		Query q = em.createQuery(jpql, Persona.class);
		q.setParameter("filtro", "%"+filtroBusqued+"%");
		
		List<Persona> personas = q.getResultList();
		return personas;
	}
	
	
}
