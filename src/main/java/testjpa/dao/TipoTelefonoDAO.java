package testjpa.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import testjpa.modelo.TipoTelefono;
import testjpa.modelo.TipoTelefono;

@Stateless
public class TipoTelefonoDAO {

	@Inject
	private EntityManager em;
	
	public void insert(TipoTelefono tipo) {
		em.persist(tipo);
	}
	
	
	public TipoTelefono read(int codigo) {
		return em.find(TipoTelefono.class, codigo);
	}
	
	public List<TipoTelefono> getTipoTelefonos(){
		String jpql = "SELECT p FROM TipoTelefono p ";
		
		Query q = em.createQuery(jpql, TipoTelefono.class);
		
		List<TipoTelefono> TipoTelefonos = q.getResultList();
		return TipoTelefonos;
	}
	
	
	
}
